# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from random import shuffle, randint
from copy import deepcopy


class TSP:
    def __init__(self, data, n):
        self.n = n
        self.matrix = data

    def display(self):
        print("n:", self.n)
        j = 0
        s = '  '
        for i in range(len(self.matrix)):
            s += ' %s ' % i
        print(s)
        for i in self.matrix:
            print(j, i)
            j += 1

    def generate_random(self):
        x = [i for i in range(self.n)]
        # shuffle(x)
        return x

    # e : list of point in the order of the link
    # [2,1,3] > From 2, go to 1 then go to 3
    def energy(self, e):
        res = 0
        for i in range(0, len(e)-1):
            res += self.matrix[e[i]][e[i+1]]
        return res + self.matrix[e[0]][e[-1]]

    def neighbor(self, e):
        i = randint(0, len(e)-1)
        b = True
        while b:
            j = randint(0, len(e)-1)
            b = j == i
        di = min(i, j)
        dj = max(i, j)
        res = deepcopy(e)
        while di <= dj:
            tmp = res[di]
            res[di] = res[dj]
            res[dj] = tmp
            di += 1
            dj -= 1
        return res
