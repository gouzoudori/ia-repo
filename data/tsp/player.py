# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from tsp import TSP
from anneal import Anneal
from lahc import Lahc
from math import sqrt
import sys


def dist_cart(a, b):
    return sqrt(pow(a[0] - b[0], 2) + pow(a[1] - b[1], 2))


def create_TSP_value(data, n):
    return [[0 if i == j else dist_cart(data[j], data[i]) for i in range(n)]
            for j in range(n)]


def solve(n, data):
    data_TSP = create_TSP_value(data, n)
    # for i in data_TSP:
    #     print(i)
    tsp = TSP(data_TSP, n)
    an = Anneal(250, 800, tsp)
    la = Lahc(10000, tsp)
    res_an, res_an_val = an.annealing()
    res_la, res_la_val = la.lahc()
    print("Bag with anneal: ", res_an)
    print("Bag with lahc: ", res_la)
    print("Travel length with anneal: %d" % res_an_val)
    print("Travel length with lahc: %d" % res_la_val)


if __name__ == "__main__":
    if len(sys.argv) != 1:
        file_location = sys.argv[1].strip()
        with open(file_location, 'r') as f:
            lines = [i.strip(' ') for i in f.read().splitlines()]
            data = [[float(j) for j in i.split(' ')] for i in lines]
            n = int(data.pop(0).pop())  # take information of first line
        solve(n, data)
    else:
        print("Need one input. Ex: `python player.py data_file`")
