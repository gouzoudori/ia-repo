# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from random import randint
import copy


class Knapsack:
    def __init__(self, data, size):
        self.data = data
        self.size = size

    def generate_random(self):
        res = copy.deepcopy(self.data)
        for i in res:
            i.insert(0, 0)
        return res

    def neighbor(self, elem):
        e = copy.deepcopy(elem)
        i = randint(0, len(e)-1)
        while e[i][0] != 0:
            i = randint(0, len(e)-1)
        e[i][0] = (e[i][0] + 1) % 2
        while (self.weight(e) > self.size):
            # print(e, " >>>",self.weight(e), "<<<  =  ", self.energy(e))
            i = randint(0, len(e)-1)
            while e[i][0] != 1:
                i = randint(0, len(e)-1)
            e[i][0] = (e[i][0] + 1) % 2
        # print(">>> ", e, " >>>",self.weight(e), "<<<  =  ", self.energy(e))
        return e

    def energy(self, e):
        return sum([i[0] * i[1] for i in e])

    def weight(self, e):
        return sum([i[0] * i[2] for i in e])
