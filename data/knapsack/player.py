# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from knapsack import Knapsack
from anneal import Anneal
from lahc import Lahc
import sys


def solve(n, m, data):
    knack = Knapsack(data, m)
    an = Anneal(200, 8000, knack)
    la = Lahc(10000, knack)
    res_an, res_an_val = an.annealing()
    res_la, res_la_val = la.lahc()
    # print("Bag with anneal: ", res_an)
    # print("Bag with lahc: ", res_la)
    print("Bag weight with anneal: %d" % res_an_val)
    print("Bag weight with lahc: %d" % res_la_val)


if __name__ == "__main__":
    if len(sys.argv) != 1:
        file_location = sys.argv[1].strip()
        with open(file_location, 'r') as f:
            lines = [i.strip(' ') for i in f.read().splitlines()]
            data = [[int(j) for j in i.split(' ')] for i in lines]
            n, m = data.pop(0)  # take information of first line
        solve(n, m, data)
    else:
        print("Need one input. Ex: `python player.py data_file`")
