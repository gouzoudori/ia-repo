import time


start = time.time()
with open("ks_10000_0", 'r') as f:
    lines = f.read().splitlines()
    b = [[int(j) for j in i.split(' ')] for i in lines]
end = time.time()
print(b)


start2 = time.time()
a, b = [int(i) for i in input().split()]
lst = [[int(i) for i in input().split()] for _ in range(a)]
end2 = time.time()
print(lst)
print("1:", end - start)
print("2:", end2 - start2)
