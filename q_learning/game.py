# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from random import randint, seed

# indicatot
MIN_ROW = 2
MAX_ROW = 6
MIN_COL = 2
MAX_COL = 6
# default value
COL = 3
ROW = 3
RAND = True
MALUS = 2
BONUS = 1
MAX_BONUS = 2


class Game:
    def __init__(self, row=ROW,  col=COL, rand=RAND, bonus=BONUS,
                 malus=MALUS, max_bonus=MAX_BONUS):
        if not rand:
            seed(6)
        self.row = row
        self.col = col
        # self.grid = [[0,0,1], [0,-1,0], [0,0,0]]
        self.grid = [[0 for _ in range(row)] for _ in range(col)]
        for i in range(malus):
            b = True
            while b:
                i = randint(0, col-1)
                j = randint(0, row-1)
                b = self.grid[i][j] != 0
            self.grid[i][j] = -randint(1, max_bonus)
        for i in range(bonus):
            b = True
            while b:
                i = randint(0, col-1)
                j = randint(0, row-1)
                b = self.grid[i][j] != 0
            self.grid[i][j] = randint(1, max_bonus)
        self.actions = [
            [-1, 0],  # Up
            [1, 0],  # Down
            [0, -1],  # Left
            [0, 1]  # Right
        ]
        self.nb_action = 4
        self.reset()

    def reset(self):
        self.x = 0
        self.y = 2
        return (self.y*self.col + self.x)

    def make_step(self, action):
        """
        0: up
        1: down
        2: left
        3: right
        """
        self.y = max(0, min(self.y + self.actions[action][0], self.row-1))
        self.x = max(0, min(self.x + self.actions[action][1], self.col-1))
        return (self.y*self.col + self.x), self.grid[self.y][self.x]

    def is_finished(self):
        return self.grid[self.y][self.x] >= 1

    def display(self):
        print("===")
        for row in self.grid:
            s = "| "
            for cell in row:
                s += "%d | " % cell
            print(s)
