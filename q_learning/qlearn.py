# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from random import uniform, randint

MAX_ITERATION = 100
LEARNING_RECT = 0.1
EXPLORE = 0.1
DISPLAY = True
GAMMA = 0.9


def take_action(state, q_learn, eps, nb_action):
    if uniform(0, 1) < eps:
        action = randint(0, nb_action-1)
    else:
        action = q_learn[state].index(max(q_learn[state]))
    return action


class QLearn:
    def __init__(self, env, max_iteration=MAX_ITERATION,
                 explore=EXPLORE, learning_rect=LEARNING_RECT,
                 display=DISPLAY, gamma=GAMMA):
        self.env = env
        self.q_grid = [[0 for j in range(env.nb_action)]
                       for i in range(env.row * env.col)]
        self.max_iteration = max_iteration
        self.learning_rect = learning_rect
        self.gamma = gamma
        self.explore = explore
        self.display = display

    def q_learn(self):
        for i in range(MAX_ITERATION):
            # print("Iteration %d" % i)
            state = self.env.reset()
            while not self.env.is_finished():
                # print(self.env.x, self.env.y,
                #       self.env.grid[self.env.y][self.env.x])
                if self.display:
                    self.env.display()
                action = take_action(state, self.q_grid,
                                     self.explore,
                                     self.env.nb_action)
                state_plus_1, reward = self.env.make_step(action)
                action_plus_1 = take_action(state_plus_1, self.q_grid,
                                            0.0,
                                            self.env.nb_action)
                self.q_grid[state][action] = (
                    self.q_grid[state][action]
                    + self.learning_rect * (
                        reward
                        + self.gamma
                        * self.q_grid[state_plus_1][action_plus_1]
                        - self.q_grid[state][action]
                    ))
                state = state_plus_1
        for s in range(len(self.q_grid)):
            print(s, self.q_grid[s])
