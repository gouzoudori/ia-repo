# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from game import Game
from qlearn import QLearn


if __name__ == "__main__":
    new_game = Game()
    for i in new_game.grid:
        print(i)
    q = QLearn(new_game, display=False)
    q.q_learn()
