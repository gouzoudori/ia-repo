# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

MEMORY_SIZE = 500
MAX_TIME = 300
LAST_UPDATE = 800


class Lahc:
    def __init__(self, iteration, data, max_time=MAX_TIME,
                 memory_size=MEMORY_SIZE, last_update=LAST_UPDATE):
        self.iteration = iteration
        self.data = data
        self.max_time = max_time
        self.memory_size = memory_size
        self.last_update = last_update

    def lahc(self):
        n = 1
        state = self.data.generate_random()  # random seed
        f_min = self.data.energy(state)  # minimum value
        state_min = state  # minimum state (return value)
        # make a memory array, wirh all value initialized to f_min
        memory = [f_min for _ in range(self.memory_size)]
        update = 0
        while n != self.iteration and update < self.last_update:
            # neighbor generated randomly from state
            new_state = self.data.neighbor(state)
            new_energy = self.data.energy(new_state)
            f_mem = memory[n % self.memory_size]
            if f_mem <= new_energy:
                state = new_state
            memory[n % self.memory_size] = self.data.energy(state)
            if f_min <= new_energy:
                state_min = new_state  # best state
                f_min = new_energy  # best energy
                update = -1  # -1 cause +1 just after
            n += 1
            update += 1
        return state_min, f_min
