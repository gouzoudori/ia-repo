# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

from random import uniform
from math import sqrt
from copy import deepcopy
import sys
# import pdb

INERTIA = 0.7
C_TRUST_SOC = 1.95
C_TRUST_COGNITIVE = 1.95
CONTRITION = 1
PROBLEM = "max"  # "min"
DIMENSION = 2
MAX_ITERATION = DIMENSION * 10000


def default_random():
    return uniform(0, 1)


def maximisation(new, old):
    return new > old


def minimisation(new, old):
    return new < old


class Particles:
    def __init__(self, position, score, speed, max_bound, min_bound,
                 inertia=INERTIA, c_trust_soc=C_TRUST_SOC,
                 r_trust_soc=default_random,
                 c_trust_cognitive=C_TRUST_COGNITIVE,
                 r_trust_cognitive=default_random,
                 contrition=CONTRITION,
                 comparator=maximisation, sign=-1):
        self.position = position
        self.score = score
        self.speed = speed
        self.max_bound = max_bound
        self.min_bound = min_bound
        self.inertia = inertia
        self.c_trust_soc = c_trust_soc
        self.r_trust_soc = r_trust_soc
        self.c_trust_cognitive = c_trust_cognitive
        self.r_trust_cognitive = r_trust_cognitive
        self.contrition = contrition
        self.sign = sign
        self.comparator = comparator
        self.best_position = self.position
        self.best_score = self.score

    def update_speed(self):
        for i in range(len(self.speed)):
            speedi = self.speed[i]
            self.speed[i] = self.inertia * speedi \
                + self.c_trust_cognitive * self.r_trust_cognitive() \
                * (self.best_position[i] - self.position[i]) \
                + self.c_trust_soc * self.r_trust_soc() \
                * (self.best_neighbor.position[i] - self.position[i])

    def move(self, evaluate):
        for i in range(len(self.position)):
            self.position[i] = min(self.max_bound,
                                   max(self.min_bound,
                                       self.speed[i] + self.position[i]))
        self.score = evaluate(self.position)

    def update_best_position(self):
        if self.comparator(self.score, self.best_score):
            self.best_position = self.position[:]
            self.best_score = self.score

    def update_best_neighbor(self, swarm):
        best = self.sign * float("inf")
        for neighbor in swarm:
            if neighbor is not self:
                if self.comparator(neighbor.score, best):
                    self.best_neighbor = neighbor
                    best = neighbor.score


class PSO:
    def __init__(self, data, dimension=DIMENSION,
                 max_iteration=MAX_ITERATION,
                 comparator=PROBLEM):
        if comparator == "max":
            self.compare = maximisation
            self.optim = max
            self.sign = -1
        elif comparator == "min":
            self.compare = minimisation
            self.optim = min
            self.sign = 1
        else:
            print("error")
            sys.exit()
        self.data = data
        try:
            self.max_bound = data.max_bound
        except AttributeError:
            self.max_bound = float("inf")
        try:
            self.min_bound = data.min_bound
        except AttributeError:
            self.min_bound = - float("inf")
        self.dimension = dimension
        self.number_particle = 10 + int(sqrt(dimension))
        self.max_iteration = max_iteration
        self.swarm = self.generate_swarm()

    def generate_swarm(self):
        res = []
        for _ in range(self.number_particle):
            value = self.data.generate_random()
            score = self.data.evaluate(value)
            speed = self.generate_speed()
            res.append(Particles(value, score, speed,
                                 max_bound=self.max_bound,
                                 min_bound=self.min_bound,
                                 comparator=self.compare, sign=self.sign))
        return res

    def pso(self):
        iteration = -1
        condition = True
        best_global = self.sign * float("inf")
        for particle in self.swarm:
            if self.compare(particle.score, best_global):
                best_global = particle.score
                best_particle = deepcopy(particle.position)
        while condition:
            iteration += 1
            for particle in self.swarm:
                particle.update_best_position()
                particle.update_best_neighbor(self.swarm)
            for particle in self.swarm:
                particle.update_speed()
                particle.move(self.data.evaluate)
            for particle in self.swarm:
                if self.compare(particle.score, best_global):
                    best_global = particle.score
                    best_particle = deepcopy(particle.position)
            condition = self.is_terminated(iteration)
        print(best_particle)
        return best_global

    def is_terminated(self, iteration):
        return iteration < self.max_iteration

    def generate_speed(self):
        return [0 for _ in range(self.dimension)]


class Rosenbrock:
    def __init__(self, dimension=DIMENSION):
        self.max_bound = 10
        self.min_bound = -5
        self.dimension = dimension

    def generate_random(self):
        return [uniform(self.min_bound, self.max_bound)
                for _ in range(self.dimension)]

    def evaluate(self, xx):
        d = len(xx)
        int_sum = 0
        for i in range(d-1):
            xi = xx[i]
            xnext = xx[i+1]
            new = 100*(xnext-xi**2)**2 + (xi-1)**2
            int_sum = int_sum + new
        y = int_sum
        return y


class Sphere:
    def __init__(self, dimension=DIMENSION):
        self.max_bound = 5.12
        self.min_bound = -5.12
        self.dimension = dimension

    def generate_random(self):
        return [uniform(self.min_bound, self.max_bound)
                for _ in range(self.dimension)]

    def evaluate(self, xx):
        d = len(xx)
        int_sum = 0
        for i in range(d):
            xi = xx[i]
            int_sum += xi ** 2
        return int_sum


if __name__ == "__main__":
    data = Rosenbrock()  # dimension = DIMENSION
    # data = Sphere(dimension=3, comparator="min")
    pso = PSO(data, comparator="min")
    print("final", pso.pso())
