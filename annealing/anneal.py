# -*- Mode: Python; tab-width: 8; indent-tabs-mode: nil; python-indent-offset: 4 -*-
# vim:set et sts=4 ts=4 tw=80:
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# author: JackRed <jackred@tuta.io>

import math
import random

TEMP_END = 0.0000000001
ALPHA = 0.999
BALANCE = 50
LAST_UPDATE = 500
ALPHA = 0.999


def default_proba(e, e_nw, temp):
    try:
        delta = e_nw - e
        if delta > 0:
            return 1
        else:
            return math.exp(-delta / temp)
    except OverflowError:
        return 1


class Anneal:
    def __init__(self, temp, iteration, data, temp_end=TEMP_END,
                 f_proba=default_proba, alpha=ALPHA,
                 balance=BALANCE, last_update=LAST_UPDATE):
        self.temp = temp
        self.temp_end = temp_end
        self.iteration = iteration
        self.data = data
        self.f_proba = f_proba
        self.alpha = alpha
        self.balance = balance
        self.last_update = last_update

    def annealing(self):
        state = self.data.generate_random()  # random seed (current state)
        energy = self.data.energy(state)  # random seed's energy
        state_min = state  # init minimum state
        energy_min = energy  # init minimum energy (what we will check)
        update = 0
        i = 0
        while (self.temp >= self.temp_end and
               i != self.iteration and
               update < self.last_update):
            j = 0
            while j < self.balance:  # stair
                # neighbor generated randomly from state
                new_state = self.data.neighbor(state)
                new_energy = self.data.energy(new_state)
                if self.f_proba(energy, new_energy, self.temp) \
                   >= random.uniform(0, 1):
                    delta = new_energy - energy  # difference between energy
                    state = new_state
                    energy = new_energy
                    update = 0
                    if delta > 0 and new_energy > energy_min:
                        energy_min = new_energy  # best energy
                        state_min = new_state  # best state
                update += 1
                j += 1
                i += 1
            self.temp *= self.alpha
        return state_min, energy_min
